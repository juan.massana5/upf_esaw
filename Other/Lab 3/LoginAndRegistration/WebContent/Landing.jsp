<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title> TweetCar </title>
        <link rel="stylesheet" type="text/css" href="css/landing.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    </head>
    <body>
        <div class="justify-content-start align-items-center d-flex" style="height: 100vh; background-image: url('./assets/images/background/signup.jpg'); background-size: cover; background-position: bottom;">
            <div style="width:15vw"></div>
            <div class="py-5 align-items-center">
                <div class="container" id="content">
                    <c:if test="${sessionScope.user != null}">
                        <jsp:include page="LoggedIn.jsp"/>
                    </c:if>
                    <c:if test="${sessionScope.user == null}">
                        <jsp:include page="Login.jsp"/>
                    </c:if>
                </div>
            </div>
        </div>
        <script>
            //Client side validation hint functions
            function displayMessage(input, hint) {
                var inputElement = document.getElementById(input);
                var hintElement = document.getElementById(hint);

                hintElement.innerHTML = "&nbsp;";
                if (inputElement.validity.tooLong) {
                    hintElement.innerHTML = "Input is too long";
                } else if (inputElement.validity.tooShort) {
                    hintElement.innerHTML = "Input is too short";
                } else if (inputElement.validity.valueMissing) {
                    hintElement.innerHTML = "Field is required";
                } else if (inputElement.validity.patternMismatch) {
                    hintElement.innerHTML = "Incorrect input";
                } else if (inputElement.validity.typeMismatch) {
                    hintElement.innerHTML = "This field is an email";
                }
            }
            function matchPasswords(password, repeatPassword, hint) {
                var passwordElement = document.getElementById(password);
                var repeatPasswordElement = document.getElementById(repeatPassword);
                var hintElement = document.getElementById(hint);

                hintElement.innerHTML = "&nbsp;";
                if (passwordElement.value != repeatPasswordElement.value) {
                    hintElement.innerHTML = "Passwords must match";
                }
            }

            //jQuery
            $(function () {
                $('[data-toggle="popover"]').popover();
            });
        </script>
    </body>
</html>