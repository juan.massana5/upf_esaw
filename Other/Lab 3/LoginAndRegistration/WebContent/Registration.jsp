<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" session="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="card p-5" style="background-color: rgba(255, 255, 255, 0.8); width: 33vw; border-radius: 25px;">
    <form id="registrationForm" action="" method="POST" autocomplete="off">
        <img class="mb-5" src="./assets/images/logo/logo_transparent_inline.png" width="65%" style="display:block; margin:auto">
        <h3 style="text-align: center"><b>Sign Up</b></h3>
        <p class="mt-3" style="text-align: center">Already a user?<a id="LoginController" href=#> Log in here </a></p>
        <br>
        <div class="form-group row mb-2">
            <label for="username" class="col-4 col-form-label">Username<br><small class="text-danger" id="usernameHint">&nbsp;</small></label>
            <div class="col-8">
                <input class="form-control" type="text" name="username" id="username" value="${user.username}" required minlength="5" oninput="displayMessage('username','usernameHint')"/>
                <span class="text-danger"><small>${error.usernameError}</small></span>
            </div>
        </div>
        <div class="form-group row mb-2">
            <label for="email" class="col-4 col-form-label">E-mail<br><small class="text-danger" id="emailHint">&nbsp;</small></label>
            <div class="col-8">
                <input class="form-control" type="email" name="email" id="email" value="${user.email}" required minlength="5" oninput="displayMessage('email','emailHint')"/>
                <span class="text-danger"><small>${error.emailError}</small></span>
            </div>
        </div>
        <div class="form-group row mb-2">
            <label for="password" class="col-4 col-form-label">Password<i class="fa fa-question-circle fa-sm ml-3 text-muted" data-toggle="popover" data-trigger="hover" title="Password format" data-content="Must include at least 1 uppercase and 1 lowercase letter, a number, and a symbol. It must have 6 or more characters." data-placement="bottom"></i><br>
                <small class="text-danger" id="passwordHint">&nbsp;</small></label>
            <div class="col-8">
                <input class="form-control" type="password" name="password" id="password" value="" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,}$" oninput="displayMessage('password','passwordHint')"/>
            </div>
        </div>
        <div class="form-group row mb-2">
            <label class="col-4 col-form-label">Repeat Password<br><small class="text-danger" id="repeatPasswordHint">&nbsp;</small></label>
            <div class="col-8">
                <input class="form-control" type="password" id="repeatPassword" value="" required minlength="5" oninput="matchPasswords('password','repeatPassword','repeatPasswordHint')"/>
            </div>
        </div>
        <div class="form-group row mb-2">
            <label for="car" class="col-4 col-form-label">Car<br><small class="text-muted" id="carHint">(Optional)</small></label>
            <div class="col-8">
                <input class="form-control" type="text" name="car" id="car" value="${user.car}"/>
            </div>
        </div>
        <input type="hidden" name="userType" id="userType" value="BASIC"/>
        <button type="submit" class="btn btn-secondary float-right mt-4">Submit</button>
    </form>
    <button class="btn btn-success float-right mt-4" onclick="autoFill()">For DEVELOPMENT - Autofill</button>
</div>
<script>
    $(document).ready(function () {
        $("#registrationForm").submit(function (event) {
            console.log("Loading");
            $.ajaxSetup({cache: false}); //Avoids Internet Explorer caching!
            $('#content').load('RegistrationController', $("#registrationForm").serialize());
            event.preventDefault();
        });

        $("#LoginController").click(function (event) {
            $('#content').load('ContentController', {content: $(this).attr('id')});
        });
    });

    function autoFill() {
        document.getElementById('username').value = "testuser";
        document.getElementById('email').value = "testuser@tweetcar.com";
        document.getElementById('password').value = "Hello@1";
        document.getElementById('repeatPassword').value = "Hello@1";
        document.getElementById('car').value = "Porsche";
    }
</script>