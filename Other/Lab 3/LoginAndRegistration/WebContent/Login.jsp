<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="card p-5" style="background-color: rgba(255, 255, 255, 0.8); width: 33vw; border-radius: 25px;">
    <form id="loginForm" action="" method="POST" autocomplete="off">
        <img class="mb-5" src="./assets/images/logo/logo_transparent_inline.png" width="65%" style="display:block; margin:auto">
        <h3 style="text-align: center"><b>Log in</b></h3>
        <p class="mt-3" style="text-align: center">Not a user?<a id="RegistrationController" href=#> Sign up here </a></p>
        <br>

        <div class="form-group row mb-2">
            <label for="username" class="col-4 col-form-label">Username<br></label>
            <div class="col-8">
                <input class="form-control" type="text" name="username" id="username" value="${login.username}" required/>
            </div>
        </div>

        <div class="form-group row mb-2">
            <label for="password" class="col-4 col-form-label">Password<br>
                <small class="text-danger" id="passwordHint">&nbsp;</small></label>
            <div class="col-8">
                <input class="form-control" type="password" name="password" id="password" value="" required/>
            </div>
        </div>

        <c:if test="${error.credentialsError != null}">
            <div class="alert alert-warning m-0" role="alert">
                <i class="fa fa-exclamation-triangle">&nbsp;&nbsp;</i>
                ${error.credentialsError}
            </div>
        </c:if>

        <button type="submit" class="btn btn-secondary float-right mt-4">Submit</button>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#RegistrationController").click(function (event) {
            $('#content').load('ContentController', {content: $(this).attr('id')});
        });

        $("#loginForm").submit(function (event) {
            console.log("Loading");
            $.ajaxSetup({cache: false}); //Avoids Internet Explorer caching!
            $('#content').load('LoginController', $("#loginForm").serialize());
            event.preventDefault();
        });
    });
</script>