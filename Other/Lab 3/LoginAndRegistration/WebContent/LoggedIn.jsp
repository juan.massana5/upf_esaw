<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" session="true"%>

<div class="card p-5" style="background-color: rgba(255, 255, 255, 0.8); width: 33vw; border-radius: 25px;">
    <br><br>
    <h3 style="text-align: center">Welcome, <b>${sessionScope.user}</b></h3>
    <h4 style="text-align: center">You have logged in successfully.</h4>
    <br>
    <br>
    <br>
    <br>
    <button style="text-align: center" id="LogoutController" class="btn btn-secondary float-right mt-4">Logout</button>
    <br>
    <br>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#LogoutController").click(function (event) {
            $('#content').load('ContentController', {content: $(this).attr('id')});
        });
    });
</script>