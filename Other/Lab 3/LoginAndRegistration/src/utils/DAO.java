package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAO {

    private final Connection connection;
    private final Statement statement;

    public DAO() throws Exception {
        String user = "mysql";
        String password = "prac";
        String url = "jdbc:mysql://localhost:3306/ts1";
        Class.forName("com.mysql.jdbc.Driver");

        connection = DriverManager.getConnection(url, user, password);
        statement = connection.createStatement();
    }

    // execute queries
    public ResultSet executeSQL(String query) throws SQLException {
        return statement.executeQuery(query);
    }

    public int executeSQLUpdate(String query) throws SQLException {
        return statement.executeUpdate(query);
    }

    // TODO: code for updates for Assignments 2, 3 and 4.
    public void disconnectBD() throws SQLException {
        statement.close();
        connection.close();
    }
}
