package controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

import error.LoginError;
import exception.UserNotExistsException;
import models.AuthenticationService;
import models.User;
import models.UserLogin;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private AuthenticationService authenticationService;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        this.authenticationService = new AuthenticationService();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("LoginController");

        UserLogin credentials = new UserLogin();
        LoginError error = new LoginError();

        try {
            BeanUtils.populate(credentials, request.getParameterMap());

            if (credentials.isComplete()) {
                try {
                    User user = authenticationService.authenticateUser(credentials);
                    HttpSession session = request.getSession();
                    session.setAttribute("user", user.getUsername());
                    System.out.println("User stored in session");
                    RequestDispatcher dispatcher = request.getRequestDispatcher("LoggedIn.jsp");
                    dispatcher.forward(request, response);
                } catch (Exception e) {
                    if (e instanceof UserNotExistsException) {
                        error.setCredentialsError("Wrong credentials");
                        request.setAttribute("login", credentials);
                        request.setAttribute("error", error);
                        RequestDispatcher dispatcher = request.getRequestDispatcher("Login.jsp");
                        dispatcher.forward(request, response);
                    } else {
                        RequestDispatcher dispatcher = request.getRequestDispatcher("Login.jsp");
                        dispatcher.forward(request, response);
                    }
                }
            } else {
                request.setAttribute("credentials", credentials);
                RequestDispatcher dispatcher = request.getRequestDispatcher("Login.jsp");
                dispatcher.forward(request, response);

            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
