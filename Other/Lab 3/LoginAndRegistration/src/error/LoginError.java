package error;

public class LoginError {

    private String credentialsError = "";

    /*
     * CONSTRUCTOR
     */
    public LoginError() {
    }

    /*
     * GETTERS and SETTERS
     */
    public String getCredentialsError() {
        return credentialsError;
    }

    public void setCredentialsError(String credentialsError) {
        this.credentialsError = credentialsError;
    }
}
