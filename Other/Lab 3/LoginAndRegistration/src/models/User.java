package models;

public class User implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private String username = "";
    private String email = "";
    private String password = "";
    private String car = "";
    private String sex = "";
    private String country = "";
    private String description = "";
    private String userType = "";

    /*
     * CONSTRUCTOR
     */
    public User() {
    }

    /*
     * GETTERS and SETTERS
     */
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    /*
     * HELPER METHODS
     */
    //Check if all the fields are filled correctly
    public boolean isComplete() {
        return (hasValue(getUsername())
                && hasValue(getEmail())
                && hasValue(getPassword())
                && hasValue(getUserType()));
    }

    private boolean hasValue(String val) {
        return ((val != null) && (!val.equals("")));
    }
}
