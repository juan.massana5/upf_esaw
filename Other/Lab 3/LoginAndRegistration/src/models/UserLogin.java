package models;

public class UserLogin implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private String username = "";
    private String password = "";

    /*
     * CONSTRUCTOR
     */
    public UserLogin() {
    }

    /*
     * GETTERS and SETTERS
     */
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*
     * HELPER METHODS
     */
    //Check if all the fields are filled correctly
    public boolean isComplete() {
        return (hasValue(getUsername())
                && hasValue(getPassword()));
    }

    private boolean hasValue(String val) {
        return ((val != null) && (!val.equals("")));
    }
}
