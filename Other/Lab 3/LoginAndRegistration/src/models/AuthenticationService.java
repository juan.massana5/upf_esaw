package models;

import exception.UserNotExistsException;

public class AuthenticationService {

    private UserDAO userDAO;

    public AuthenticationService() {
        super();
        this.userDAO = new UserDAO();
    }

    /*
     * METHODS
     */
    public User authenticateUser(UserLogin credentials) throws Exception {
        User user = userDAO.findByUsernameAndPassword(credentials.getUsername(), credentials.getPassword());
        validateUserExists(user);

        return user;
    }

    /*
     * HELPER METHODS
     */
    private void validateUserExists(User user) throws UserNotExistsException {
        if (user == null) {
            throw new UserNotExistsException();
        }
    }
}
