<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="models.User"%>

<!DOCTYPE html>
<html>
<head>
<title> Template Register Form (Validation JQuery) </title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script>
$(document).ready(function(){
    $("#registerForm").validate();
  });
</script>
</head>
<body>

<% 
/* This java code is only for debbuging purposes it avoids errors when calling 
   the view before the controller. Using the MVC design pattern this will not occur
   and this java code can be safely removed. */
User user = null;
if (request.getAttribute("user")!=null) 
	user = (User)request.getAttribute("user");
else 
	user = new User();
%>

<form action="RegisterFormController" method="post" id="registerForm">
<fieldset>
<p>
<label> User id (Required, minimum 5 characters) </label>
<input type="text" name="usr" id="usr" value="<%=user.getUsr() %>" required minlength="5"/>
<% 	if ( user.getError()[0] == 1) 
		out.println("The username already exists in our DB!");
%>
</p>
<p>
<label> E-mail (Required, valid e-mail adress) </label>
<input type="email" name="mail" id="mail" value="<%=user.getMail() %>" required email/>
</p>
<p>
<label> Password (min 5 characters including a digit and an upper letter) </label>
<input type="password" name="pwd" id="pwd" value="<%=user.getPwd() %>" required pattern="^(?=.*[0-9])(?=.*?[A-Z])(?=.*?[a-z]).{5,}$"/>
</p>
<input type="submit" value="Enviar"> 
</fieldset>
</form>
</body>
</html>