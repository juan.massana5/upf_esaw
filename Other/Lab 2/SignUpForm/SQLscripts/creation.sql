CREATE USER IF NOT EXISTS 'mysql'@'localhost' IDENTIFIED BY 'prac';
GRANT ALL PRIVILEGES ON *.* TO 'mysql'@'localhost';

DROP DATABASE IF EXISTS ts1;
CREATE DATABASE ts1;
USE ts1;

CREATE TABLE `taula` (
    `id` int(12) NOT NULL AUTO_INCREMENT,
    `username` char(24) NOT NULL,
    `email` char(24) NOT NULL,
    `password` char(24) NOT NULL,
    `car` char(24) DEFAULT NULL,
    `sex` char(24) DEFAULT NULL,
    `country` char(24) DEFAULT NULL,
    `description` char(24) DEFAULT NULL,
    `userType` char(24) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE(`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;