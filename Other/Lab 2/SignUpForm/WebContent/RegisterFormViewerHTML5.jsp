<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" import="models.User, errors.SignUpError"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Template Register Form (Validation HTML5)</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <%
            User user = null;
            if (request.getAttribute("user") != null) {
                user = (User) request.getAttribute("user");
            } else {
                user = new User();
            }

            SignUpError error = null;
            if (request.getAttribute("error") != null) {
                error = (SignUpError) request.getAttribute("error");
            } else {
                error = new SignUpError();
            }
        %>
        <div class="justify-content-start align-items-center d-flex" style="height: 100vh; background-image: url('./assets/images/background/signup.jpg'); background-size: cover; background-position: bottom;">
            <div style="width:15vw"></div>
            <div class="py-5 align-items-center">
                <div class="container">
                    <div class="card p-5" style="background-color: rgba(255, 255, 255, 0.8); width: 33vw; border-radius: 25px;">
                        <form action="registration" method="post" id="registerForm" autocomplete="off">

                            <img class="mb-5" src="./assets/images/logo/logo_transparent_inline.png" width="65%" style="display:block; margin:auto">
                            <h3 style="text-align: center"><b>Sign Up</b></h3>
                            <br>
                            <br>
                            <br>

                            <div class="form-group row mb-2">
                                <label for="username" class="col-4 col-form-label">Username<br><small class="text-danger" id="usernameHint">&nbsp;</small></label>
                                <div class="col-8">
                                    <input class="form-control" type="text" name="username" id="username" value="<%=user.getUsername()%>" required minlength="5" oninput="displayMessage('username','usernameHint')"/>
                                    <span class="text-danger"><small><%=error.getUsernameError()%></small></span>
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label for="email" class="col-4 col-form-label">E-mail<br><small class="text-danger" id="emailHint">&nbsp;</small></label>
                                <div class="col-8">
                                    <input class="form-control" type="email" name="email" id="email" value="<%=user.getEmail()%>" required minlength="5" oninput="displayMessage('email','emailHint')"/>
                                    <span class="text-danger"><small><%=error.getEmailError()%></small></span>
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label for="password" class="col-4 col-form-label">Password<i class="fa fa-question-circle fa-sm ml-3 text-muted" data-toggle="popover" data-trigger="hover" title="Password format" data-content="Must include at least 1 uppercase and 1 lowercase letter, a number, and a symbol. It must have 6 or more characters." data-placement="bottom"></i><br>
                                    <small class="text-danger" id="passwordHint">&nbsp;</small></label>
                                <div class="col-8">
                                    <input class="form-control" type="password" name="password" id="password" value="" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{6,}$" oninput="displayMessage('password','passwordHint')"/>
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label class="col-4 col-form-label">Repeat Password<br><small class="text-danger" id="repeatPasswordHint">&nbsp;</small></label>
                                <div class="col-8">
                                    <input class="form-control" type="password" id="repeatPassword" value="" required minlength="5" oninput="matchPasswords('password','repeatPassword','repeatPasswordHint')"/>
                                </div>
                            </div>

                            <div class="form-group row mb-2">
                                <label for="car" class="col-4 col-form-label">Car<br><small class="text-muted" id="carHint">(Optional)</small></label>
                                <div class="col-8">
                                    <input class="form-control" type="text" name="car" id="car" value="<%=user.getCar()%>"/>
                                </div>
                            </div>

                            <input type="hidden" name="userType" id="userType" value="BASIC"/>

                            <button type="submit" class="btn btn-secondary float-right mt-4">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            //Client side validation hint functions
            function displayMessage(input, hint) {
                var inputElement = document.getElementById(input);
                var hintElement = document.getElementById(hint);

                hintElement.innerHTML = "&nbsp;";
                if (inputElement.validity.tooLong) {
                    hintElement.innerHTML = "Input is too long";
                } else if (inputElement.validity.tooShort) {
                    hintElement.innerHTML = "Input is too short";
                } else if (inputElement.validity.valueMissing) {
                    hintElement.innerHTML = "Field is required";
                } else if (inputElement.validity.patternMismatch) {
                    hintElement.innerHTML = "Incorrect input";
                } else if (inputElement.validity.typeMismatch) {
                    hintElement.innerHTML = "This field is an email";
                }
            }
            function matchPasswords(password, repeatPassword, hint) {
                var passwordElement = document.getElementById(password);
                var repeatPasswordElement = document.getElementById(repeatPassword);
                var hintElement = document.getElementById(hint);

                hintElement.innerHTML = "&nbsp;";
                if (passwordElement.value != repeatPasswordElement.value) {
                    hintElement.innerHTML = "Passwords must match";
                }
            }

            //jQuery
            $(function () {
                $('[data-toggle="popover"]').popover();
            });
        </script>
    </body>
</html>