package models;

import java.util.regex.Pattern;

public class User implements java.io.Serializable   {

	private static final long serialVersionUID = 1L;

	private String usr 	= "";
	private String mail = "";
	private String pwd 	= "";
	
	private Pattern pwdpattern = Pattern.compile("^(?=.*[0-9])(?=.*?[A-Z])(?=.*?[a-z]).{5,}$");
	
	/*  Control flag, shows the parameters that have been correctly filled */
	private int[] error = {0,0,0}; 
	
	public static void main() {
	}

	public String getUsr() {
		return this.usr;
	}
	
	public String getMail() {
		return this.mail;
	}
	
	public String getPwd() {
		return this.pwd;
	}
	
	public int[] getError() {
		return error;
	}
	
	public void setUsr(String usr) {
		System.out.println("Filling user field");
		/* We simulate that a user with the same name exists in our DB and mark error[0] as failed  */
		error[0] = 1;
		//this.usr = usr;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
		
	public void setPwd(String pwd) {
		if (pwdpattern.matcher(pwd).matches()) {
			this.pwd = pwd;
			System.out.println("Password pattern matched");
		}
		else {
			System.out.println("Password pattern not matched");
		}
	}
	
	/*Check if all the fields are filled correctly */
	public boolean isComplete() {
	    return(hasValue(getUsr()) &&
	    	   hasValue(getMail()) &&
	           hasValue(getPwd()) );
	}
	
	private boolean hasValue(String val) {
		return((val != null) && (!val.equals("")));
	}
	
	
}
