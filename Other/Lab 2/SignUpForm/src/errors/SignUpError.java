package errors;

public class SignUpError {

    private String usernameError = "";
    private String emailError = "";

    /*
     *  CONSTRUCTOR
     */
    public SignUpError() {
    }

    /*
     *  GETTERS and SETTERS
     */
    public String getUsernameError() {
        return usernameError;
    }

    public void setUsernameError(String usernameError) {
        this.usernameError = usernameError;
    }

    public String getEmailError() {
        return emailError;
    }

    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }
}
