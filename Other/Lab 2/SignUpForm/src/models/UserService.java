package models;

import exception.EmailAlreadyExistsException;
import exception.UsernameAlreadyExistsException;

public class UserService {

    private UserDAO userDAO;

    public UserService() {
        super();
        this.userDAO = new UserDAO();
    }

    /*
     * METHODS
     */
    public void registerUser(User user) throws Exception {
        validateUsernameNotExists(user.getUsername());
        validateEmailNotExists(user.getEmail());

        userDAO.save(user);
    }

    /*
     * HELPER METHODS
     */
    private void validateUsernameNotExists(String username) throws UsernameAlreadyExistsException {
        User user = userDAO.findByUsername(username);
        if (user != null) {
            throw new UsernameAlreadyExistsException();
        }
    }

    private void validateEmailNotExists(String email) throws EmailAlreadyExistsException {
        User user = userDAO.findByEmail(email);
        if (user != null) {
            throw new EmailAlreadyExistsException();
        }
    }
}
