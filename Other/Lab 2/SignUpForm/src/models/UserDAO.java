package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import utils.DAO;

public class UserDAO {

    private DAO dao;

    /*
     *  CONSTRUCTOR
     */
    public UserDAO() {
        try {
            this.dao = new DAO();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*
     *  METHODS
     */
    public void save(User user) {
        try {
            dao.executeSQLUpdate(
                    "INSERT INTO `taula` (`userName`, `email`, `password`, `car`, `sex`, `country`, `description`, `userType`) \n"
                    + " VALUES \n"
                    + "('" + user.getUsername() + "',"
                    + "'" + user.getEmail() + "',"
                    + "'" + user.getPassword() + "',"
                    + "'" + user.getCar() + "',"
                    + "'" + user.getSex() + "',"
                    + "'" + user.getCountry() + "',"
                    + "'" + user.getDescription() + "',"
                    + "'" + user.getUserType() + "') ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User findByUsername(String username) {
        User user = null;

        try {
            ResultSet resultSet = dao.executeSQL("SELECT * FROM taula WHERE username = '" + username + "'");
            while (resultSet.next()) {
                user = mapResultRowToUser(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    public User findByEmail(String email) {
        User user = null;

        try {
            ResultSet resultSet = dao.executeSQL("SELECT * FROM taula WHERE email = '" + email + "'");
            while (resultSet.next()) {
                user = mapResultRowToUser(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    /*
     *  HELPER METHODS
     */
    private User mapResultRowToUser(ResultSet resultSet) throws SQLException {
        User user = new User();

        user.setUsername(resultSet.getString("username"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        user.setCar(resultSet.getString("car"));
        user.setSex(resultSet.getString("sex"));
        user.setCountry(resultSet.getString("country"));
        user.setDescription(resultSet.getString("description"));
        user.setUserType(resultSet.getString("userType"));

        return user;
    }
}
