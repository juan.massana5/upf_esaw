package controllers;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import errors.SignUpError;
import exception.EmailAlreadyExistsException;
import exception.UsernameAlreadyExistsException;
import models.User;
import models.UserService;

/**
 * Servlet implementation class RegisterFormController
 */
@WebServlet("/registration")
public class RegisterFormController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private UserService userService;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterFormController() {
        super();
        this.userService = new UserService();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User();
        SignUpError error = new SignUpError();

        try {
            // Fills the User Bean with the parameters that are passed into the request
            BeanUtils.populate(user, request.getParameterMap());

            if (user.isComplete()) {
                try {
                    userService.registerUser(user);
                } catch (Exception e) {
                    if (e instanceof UsernameAlreadyExistsException) {
                        error.setUsernameError("Username taken. Please try again.");
                        request.setAttribute("user", user);
                        request.setAttribute("error", error);
                        RequestDispatcher dispatcher = request.getRequestDispatcher("/RegisterFormViewerHTML5.jsp");
                        dispatcher.forward(request, response);
                    } else if (e instanceof EmailAlreadyExistsException) {
                        error.setEmailError("Email address taken. Please try again.");
                        request.setAttribute("user", user);
                        request.setAttribute("error", error);
                        RequestDispatcher dispatcher = request.getRequestDispatcher("/RegisterFormViewerHTML5.jsp");
                        dispatcher.forward(request, response);
                    } else {
                        request.setAttribute("user", user);
                        RequestDispatcher dispatcher = request.getRequestDispatcher("/RegisterFormViewerHTML5.jsp");
                        dispatcher.forward(request, response);
                    }
                }
            } else {
                request.setAttribute("user", user);
                RequestDispatcher dispatcher = request.getRequestDispatcher("/RegisterFormViewerHTML5.jsp");
                dispatcher.forward(request, response);
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
