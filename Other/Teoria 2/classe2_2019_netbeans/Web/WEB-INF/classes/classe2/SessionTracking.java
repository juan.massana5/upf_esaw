package classe2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utils.ServletUtilities;

/**
 * Servlet implementation class SessionTracking
 */
@WebServlet("/SessionTracking")
public class SessionTracking extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionTracking() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        String heading;
        String title = "Session Tracking";
        Integer accessCount;

        synchronized (session) {

            accessCount = (Integer) session.getAttribute("accessCount");
            if (accessCount == null) {
                accessCount = 0;
                heading = "Welcome, Newcomer";
                //Cookie cookie = new Cookie("JSESSIONID", session.getId());
                //cookie.setMaxAge(5*60);
                //response.addCookie(cookie);

            } else {
                heading = "Welcome Back";
                accessCount = accessCount + 1;
            }

            session.setAttribute("accessCount", accessCount);
        }
        PrintWriter out = response.getWriter();
        out.println(ServletUtilities.headWithTitle(title)
                + "<body>\n"
                + "<H1>" + heading + "</H1>\n"
                + "<H2>Information on Your Session:</H2>\n"
                + "<TABLE BORDER=1>\n"
                + "<TR>\n"
                + "<TH>Info Type<TH>Value\n"
                + "<TR>\n"
                + "  <TD>ID\n"
                + "  <TD>" + session.getId() + "\n"
                + "<TR>\n"
                + "  <TD>Creation Time\n"
                + "  <TD>"
                + new Date(session.getCreationTime()) + "\n"
                + "<TR>\n"
                + "  <TD>Time of Last Access\n"
                + "  <TD>"
                + new Date(session.getLastAccessedTime()) + "\n"
                + "<TR>\n"
                + "<TD>Number of Previous Accesses\n"
                + "<TD>" + accessCount + "\n"
                + "</TABLE>\n"
                + "</BODY></HTML>");

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
