package classe2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.ServletUtilities;

/**
 * Servlet implementation class Parametres
 */
@WebServlet("/Parametres")
public class Parametres extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Parametres() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Lectura de paràmetres";
        out.println(ServletUtilities.headWithTitle(title)
                + "<body>\n"
                + "<h1>" + title + "</h1>\n" + "<UL>\n"
                + " <li><b>param1</b>: "
                + request.getParameter("param1") + "\n" + " <li><b>param2</b>: "
                + request.getParameter("param2") + "\n" + " <li><b>param3</b>: "
                + request.getParameter("param3") + "\n" + "</ul>\n"
                + "</body></html>");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }
}
