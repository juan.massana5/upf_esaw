CREATE USER IF NOT EXISTS 'mysql'@'localhost' IDENTIFIED BY 'prac';
GRANT ALL PRIVILEGES ON *.* TO 'mysql'@'localhost';

DROP DATABASE IF EXISTS ts1;
CREATE DATABASE ts1;
USE ts1;

CREATE TABLE `taula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` char(24) DEFAULT NULL,
  `descripcio` varchar(255) DEFAULT NULL,
  `telefon` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;