USE ts1;

INSERT INTO `taula` (`nom`, `descripcio`, `id`, `telefon`)
VALUES
    ('Bailey Dicki','Cumque dolores quia earum error cumque vero.','1','257'),
    ('Jonas Lynch','Nisi delectus hic porro.','2','0'),
    ('Ms. Kailyn Rutherford MD','Vel nisi non aut voluptatem eveniet et.','3','1'),
    ('Noelia Gulgowski','Cupiditate asperiores quo recusandae minus quo corporis.','4','180673'),
    ('Mr. Lula Osinski','Est corporis suscipit necessitatibus cum placeat ab eos.','5','1'),
    ('Mrs. Fleta Kuphal Sr.','Debitis aliquam iure quia ipsa debitis pariatur et.','6','767871'),
    ('Prof. Tyrese Mitchell DV','Dolorum quo consequatur vel cumque qui.','7','330653'),
    ('Charity Bosco','Sed voluptate quia laborum accusamus culpa.','8','479'),
    ('Mr. Oran Botsford V','Aut odit modi et ut excepturi impedit numquam.','9','1'),
    ('Micah Schimmel','Voluptas omnis accusamus commodi qui sit laudantium.','10','942'),
    ('Prof. Vito Quigley','Sunt sint quis quaerat et.','11','29'),
    ('Prof. Montana Brown','Laborum explicabo nemo atque consequuntur ut aut.','12','803691'),
    ('Kristin Mueller','Inventore natus eum sunt non.','13','2147483647'),
    ('Dr. Wilburn Heaney','Omnis perferendis eum error.','14','309'),
    ('Florine Runolfsson MD','Eius voluptatem qui dolorem est quia dicta.','15','832239');