package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.ServletUtilities;

/**
 * Servlet implementation class HelloWorld
 */
@WebServlet("/helloworld")
public class HelloWorld extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public HelloWorld() {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println(ServletUtilities.headWithTitleAndStyles("Hello World!") + "<body>\n"
                + "<h1>Hello World</h1>\n"
                + "<span><a href=\"" + request.getContextPath() + "/printdb\"" + ">Go to <b>PrintDB Table</b> (Part 2 of Lab) <i class=\"fa fa-arrow-circle-right fa-lg\"></i></a></span>"
                + "<span><a href=\"" + request.getContextPath() + "\"> Go to <b>Index</b> <i class=\"fa fa-arrow-circle-up fa-lg\"></i></a></span>"
        		+ "</br>\n"
        		+ "</br>\n"
        		+ "</br>\n"
        		+ "</br>\n"
        		+ "</br>\n"
                + "<h1>\n"
                + "This is a dynamic web project for Lab 1 of"
                + "</br>\n"
        		+ "</br>\n"
        		+ "<span style=\"font-size: 180%\">Software Engineering for Web Applications</span>\n"
        		+ "</h1>\n"                
                + "</body>");
    }   

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
