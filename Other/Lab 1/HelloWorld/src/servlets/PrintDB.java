package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.DAO;
import utils.ServletUtilities;

/**
 * Servlet implementation class PrintDB
 */
@WebServlet("/printdb")
public class PrintDB extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private DAO dao;

    public PrintDB() {
        super();
        try {
            this.dao = new DAO();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ResultSet queryResults = getTableContents();
        printPage(queryResults, request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    /*	HELPER METHODS  */
    private void printPage(ResultSet queryResults, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String bodyContent = "<h1>PrintDB</h1>";
        bodyContent = bodyContent.concat("<span><a href=\"" + request.getContextPath() + "/helloworld\"" + ">Go to <b>Hello World</b> (Part 1 of Lab) <i class=\"fa fa-arrow-circle-right fa-lg\"></i></a></span>");
        bodyContent = bodyContent.concat("<span><a href=\"" + request.getContextPath() + "\"> Go to <b>Index</b> <i class=\"fa fa-arrow-circle-up fa-lg\"></i></a></span></br></br>");
        bodyContent = bodyContent.concat(createHTMLtable(queryResults));

        String webpage = ServletUtilities.headWithTitleAndStyles("PrintDB");
        webpage = webpage.concat(ServletUtilities.htmlBody(bodyContent));

        PrintWriter out = response.getWriter();
        out.println(webpage);
    }

    private String createHTMLtable(ResultSet queryResults) {
        String table = "<table>\n";

        try {
            ResultSetMetaData rowMetaData = queryResults.getMetaData();
            Integer columnCount = rowMetaData.getColumnCount();

            table = table.concat("<tr>\n");
            for (Integer i = 1; i <= columnCount; ++i) {
                table = table.concat("<th>" + rowMetaData.getColumnName(i) + "</th>\n");
            }
            table = table.concat("</tr>\n");

            while (queryResults.next()) {
                table = table.concat("<tr>\n");
                for (Integer i = 1; i <= columnCount; ++i) {
                    table = table.concat("<td>" + queryResults.getString(i) + "</td>\n");
                }
                table = table.concat("</tr>\n");
            }
        } catch (SQLException e) {
            table = table.concat("<tr>\n");
            table = table.concat("<th colspan=\"4\">" + "ERROR retrieving contents" + "</th>\n");
            table = table.concat("</tr>\n");
        }

        table = table.concat("</table>\n");

        return table;
    }

    private ResultSet getTableContents() {
        ResultSet queryResults = null;

        try {
            queryResults = dao.executeSQL("SELECT * FROM taula");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return queryResults;
    }
}
