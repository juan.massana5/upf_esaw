package utils;

import javax.servlet.http.*;

/**
 * Some simple time savers. Static methods.
 */
public class ServletUtilities {

    public static String headWithTitle(String title) {
        return ("<!DOCTYPE html>\n"
                + "<html>\n"
                + "<meta charset=\"UTF-8\">\n"
                + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"
                + "<head><title>" + title + "</title>"
                + "</head>\n");
    }

    public static String headWithTitleAndStyles(String title) {
        return ("<!DOCTYPE html>\n"
                + "<html>\n"
                + "<meta charset=\"UTF-8\">\n"
                + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"
                + "<head><title>" + title + "</title>"
                + getStyles()
                + "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.8.1/css/all.css\" integrity=\"sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf\" crossorigin=\"anonymous\">\n"
                + "</head>\n");
    }

    public static String htmlBody(String body) {
        return ("<body>\n"
                + body
                + "</body>\n");
    }

    /**
     * Read a parameter with the specified name, convert it to an int, and
     * return it. Return the designated default value if the parameter doesn't
     * exist or if it is an illegal integer format.
     */
    public static int getIntParameter(HttpServletRequest request,
            String paramName,
            int defaultValue) {
        String paramString = request.getParameter(paramName);
        int paramValue;
        try {
            paramValue = Integer.parseInt(paramString);
        } catch (Exception nfe) { // null or bad format
            paramValue = defaultValue;
        }
        return (paramValue);
    }

    /**
     * Reads param and converts to double. Default if problem.
     */
    public static double getDoubleParameter(HttpServletRequest request,
            String paramName,
            double defaultValue) {
        String paramString = request.getParameter(paramName);
        double paramValue;
        try {
            paramValue = Double.parseDouble(paramString);
        } catch (Exception nfe) { // null or bad format
            paramValue = defaultValue;
        }
        return (paramValue);
    }

    /**
     * Replaces characters that have special HTML meanings with their
     * corresponding HTML character entities. Specifically, given a string, this
     * method replaces all occurrences of {
     * @literal '<' with '&lt;', all occurrences of '>' with
     *  '&gt;', and (to handle cases that occur inside attribute
     *  values), all occurrences of double quotes with
     *  '&quot;' and all occurrences of '&' with '&amp;'.
     *  Without such filtering, an arbitrary string
     *  could not safely be inserted in a Web page.
     * }
     */
    public static String filter(String input) {
        if (!hasSpecialChars(input)) {
            return (input);
        }
        StringBuilder filtered = new StringBuilder(input.length());
        char c;
        for (int i = 0; i < input.length(); i++) {
            c = input.charAt(i);
            switch (c) {
                case '<':
                    filtered.append("&lt;");
                    break;
                case '>':
                    filtered.append("&gt;");
                    break;
                case '"':
                    filtered.append("&quot;");
                    break;
                case '&':
                    filtered.append("&amp;");
                    break;
                default:
                    filtered.append(c);
            }
        }
        return (filtered.toString());
    }

    private static boolean hasSpecialChars(String input) {
        boolean flag = false;
        if ((input != null) && (input.length() > 0)) {
            char c;
            for (int i = 0; i < input.length(); i++) {
                c = input.charAt(i);
                switch (c) {
                    case '<':
                        flag = true;
                        break;
                    case '>':
                        flag = true;
                        break;
                    case '"':
                        flag = true;
                        break;
                    case '&':
                        flag = true;
                        break;
                }
            }
        }
        return (flag);
    }

    private ServletUtilities() {
    } // Uninstantiatable class

    private static String getStyles() {
        String styles = "<style>\n"
                + "body {\n"
                + "padding-left: 20%;\n"
                + "padding-right: 20%;\n"
                + "padding-bottom: 5%;\n"
                + "padding-top: 5%;\n"
                + "background-color: #C81530;\n"
                + "color: white;\n"
                + "font-family: Verdana, Geneva, sans-serif;\n"
                + "}\n"
                + "body a {\n"
                + "display: block;\n"
                + "margin-bottom: 10px;\n"  
                + "color: white;\n"
                + "text-decoration: none;\n"          
                + "}\n" 
                + "table {\n"
                + "font-family: arial, sans-serif;\n"
                + "border-collapse: collapse;\n"
                + "width: 100%;\n"
                + "background-color: #ffffff;\n"
                + "color: #000000;\n"
                + "}\n"
                + "td, th {\n"
                + "border: 1px solid #dddddd;\n"
                + "text-align: left;\n"
                + "padding: 8px;\n"
                + "}\n"
                + "th {\n"
                + "background-color: #930f23;\n"
                + "color: #ffffff;\n"
                + "}\n"
                + "tr:hover {\n"
                + "background-color: #fce0e4;\n"
                + "}\n"                
                + "</style>\n";

        return styles;
    }
}
