package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAO {

    private final Connection connection;
    private final Statement statement;

    public DAO() throws Exception {
        String user = "mysql";
        String password = "prac";
        Class.forName("com.mysql.jdbc.Driver");

        connection = DriverManager.getConnection("jdbc:mysql://localhost/ts1?user=" + user + "&password=" + password);
        statement = connection.createStatement();
    }

    //execute queries
    public ResultSet executeSQL(String query) throws SQLException {
        ResultSet resultSet = statement.executeQuery(query);        
        disconnectBD();
        
    	return resultSet;
    }

    //TODO: code for updates for Assignments 2, 3 and 4.
    public void disconnectBD() throws SQLException {
        statement.close();
        connection.close();
    }
}
